import { Vpc } from "./lib/vpc";
import { Eks } from "./lib/eks";
import { Route53 } from "./lib/route53";
import * as pulumi from "@pulumi/pulumi";

const config = new pulumi.Config();
const mainHostedZoneId: string = config.require("mainHostedZoneId");
const mainVpc = new Vpc();
const cluster = new Eks(mainVpc.Vpc);
const hostedZone = new Route53(mainHostedZoneId, pulumi.getStack());

export const wildCardCertificate = hostedZone.wildCardCertificate;
export const kubeConfig = cluster.kubeConfig;
export const clusterOidcProviderArn = cluster.clusterOidcProviderArn;
export const clusterOidcProviderUrl = cluster.clusterOidcProviderUrl;
export const vpcId = mainVpc.Vpc.id;
export const privateSubnets = mainVpc.Vpc.privateSubnetIds;
export const publicSubnets = mainVpc.Vpc.publicSubnetIds;
export const domainName = hostedZone.pocHostedZone.name;
export let publicSubnet1: any = "";
export let publicSubnet2: any = "";
export let publicSubnet3: any = "";
export let privateSubnet1: any = "";
export let privateSubnet2: any = "";
export let privateSubnet3: any = "";

mainVpc.Vpc.publicSubnetIds.then((subnets: any) => {
  publicSubnet1 = subnets[0];
  publicSubnet2 = subnets[1];
  publicSubnet3 = subnets[2];
});

mainVpc.Vpc.publicSubnetIds.then((subnets: any) => {
  privateSubnet1 = subnets[0];
  privateSubnet2 = subnets[1];
  privateSubnet3 = subnets[2];
});