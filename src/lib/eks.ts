import * as eks from "@pulumi/eks";
import * as pulumi from "@pulumi/pulumi";
import { ec2 } from "@pulumi/awsx";

let config = new pulumi.Config();
let AWS_PROFILE = config.require("aws_profile");

export class Eks {
  kubeConfig: pulumi.Output<any>;
  clusterOidcProviderUrl: pulumi.Output<any> | undefined;
  clusterOidcProviderArn: pulumi.Output<any> | undefined;

  constructor(vpc: ec2.Vpc) {

    const cluster = new eks.Cluster("ssb-eks", {
      vpcId: vpc.id,
      instanceType: "t2.large",
      privateSubnetIds: vpc.getSubnetsIds("private"),
      publicSubnetIds: vpc.getSubnetsIds("public"),
      desiredCapacity: 3,
      maxSize: 4,
      minSize: 2,
      deployDashboard: false,
      providerCredentialOpts: {profileName: AWS_PROFILE},
      createOidcProvider: true
    });
    this.clusterOidcProviderArn = cluster.core.oidcProvider?.arn;
    this.clusterOidcProviderUrl = cluster.core.oidcProvider?.url;
    this.kubeConfig = cluster.kubeconfig.apply((kc: any) => kc);
  }
}