import * as awsx from "@pulumi/awsx";

export class Vpc {
  Vpc: awsx.ec2.Vpc;

  constructor() {
    this.Vpc = new awsx.ec2.Vpc("pulumi", {
      numberOfAvailabilityZones: 3,
    });
  }  
}