import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";

export class Route53 {
  pocHostedZone: aws.route53.Zone;
  wildCardCertificate: pulumi.Output<any>;

  constructor(mainHostedZoneId: string, subDomain: string) {
    
    //getting the main hosted zone sandeepsinghbalouria.com
    const mainDomain = "sandeepsinghbalouria.com";

    const main = aws.route53
      .getZone({ zoneId: mainHostedZoneId }, { async: true })
      .then((zone: any) => zone.id);


    // creating a subdomain
    this.pocHostedZone = new aws.route53.Zone(subDomain, {
      name: `${subDomain}.${mainDomain}`,
      tags: {
        Environment: subDomain,
      },
    });

    // adding nameservers to the main zone for new subdomain
    const poc_ns = new aws.route53.Record("poc-ns", {
      zoneId: main,
      name: `${subDomain}.${mainDomain}`,
      type: "NS",
      ttl: 30,
      records: this.pocHostedZone.nameServers,
    });

    const cert = new aws.acm.Certificate("cert", {
      domainName: `*.${subDomain}.${mainDomain}`,
      tags: {
        Environment: "POC",
      },
      validationMethod: "DNS",
    });

    this.wildCardCertificate = cert.arn;

    const certValidation = new aws.route53.Record("cert_validation", {
      name: cert.domainValidationOptions[0].resourceRecordName,
      records: [cert.domainValidationOptions[0].resourceRecordValue],
      ttl: 60,
      type: cert.domainValidationOptions[0].resourceRecordType,
      zoneId: this.pocHostedZone.zoneId!,
    });

    const certCertificateValidation = new aws.acm.CertificateValidation(
      "cert",
      {
        certificateArn: cert.arn,
        validationRecordFqdns: [certValidation.fqdn],
      }
    );
  }
}
